#include <stdio.h>
#include <stdlib.h>
#include "function.h"
#include "memory.h"
#include "display.h"
#include "main.h"


int main(int argc, char ** argv) {
    int code=0,i;

    /*Initialisation de la m�moire*/
    mot* mem;
    mem = malloc(16);
    loadMem1(mem);

    for(i=0 ; i<8 ; i++) printf("%d\n",mem[i].brut);

    /*lancement de l'�mulateur*/
    code = emulateur(mem);

    if (code == 0)
        return EXIT_SUCCESS;

    else
        return EXIT_FAILURE;
}


int emulateur(mot* mem) {
    /*Cr�ation et initialisation des registres*/
    registre reg;
    reg.PC=0;
    reg.SP=sizeof(mem)/16;

    /*Cr�ation des variables contenant les "mots" de procsi*/
    mot currentInstru;
    mot secondMot;
    mot troisiemeMot;
    /*boolean indiquant la pr�sence d'un �ventuellement second voir troisi�me mot*/
    int boolSecondMot;
    int boolTroisiemeMot;


    /*L'�mulateur tourne tant qu'il ne rencontre pas d'instruction HALT, et tant qu'il n'a pas de probl�mes*/
    while(1) {
        boolSecondMot=0;
        boolTroisiemeMot=0;

        /*affichage des registres*/
        printReg(reg);

        /*lecture de l'instruction*/
        currentInstru = mem[reg.PC];
        reg.PC ++;
        /*V�rification,a vec le mode, de la pr�sence d'un second mot*/
        if(currentInstru.codage.mode == DIRREG || currentInstru.codage.mode == REGIMM || currentInstru.codage.mode == INDIMM || currentInstru.codage.mode == REGDIR) {
            secondMot = mem[reg.PC];
            boolSecondMot=1;
            reg.PC++;
        }
        /*V�rification de la pr�sence d'un troisi�me mot*/
        if(currentInstru.codage.mode == DIRIMM) {
            troisiemeMot = mem[reg.PC];
            boolTroisiemeMot=1;
            reg.PC ++;
        }

        printInstru(currentInstru, secondMot, troisiemeMot, boolSecondMot, boolTroisiemeMot);

        /*Si on recoit l'instruction d'arr�t, on stop le programme*/
        if(currentInstru.codage.codeop == HALT) {
            printf("Instruction d'arr�t.\n\n");
            printf("Appuyez sur \"Entrer\" pour continuer ...");
            getchar();
            fflush(stdin);
            break;
        }
        /*Sinon, on v�rifie que le mode d'adressage soit bien valide*/
        else if(checkModeInstru(currentInstru)) {
            printf("Mode d'adressage invalide - PC = %d\n\n",reg.PC);
            printf("Appuyez sur \"Entrer\" pour continuer ...");
            getchar();
            fflush(stdin);
            break;
        }

        /*Sinon, on ex�cute l'instruction*/
        switch(currentInstru.codage.mode) {

            /*Mode d'adressage : registre-registre*/
            case REGREG:
                switch(currentInstru.codage.codeop) {
                    case ADD:
                        add(&reg.gen[currentInstru.codage.source], &reg.gen[currentInstru.codage.dest],&reg.SR);
                        break;
                    case SUB:
                        sub(&reg.gen[currentInstru.codage.source], &reg.gen[currentInstru.codage.dest],&reg.SR);
                        break;
                    case POP:
                        pop(&reg.gen[currentInstru.codage.dest],&reg.SP, &mem[reg.SP].brut);
                        break;
                    case PUSH:
                        push(&mem[reg.SP].brut, &reg.gen[currentInstru.codage.source],&reg.SP);
                        break;
                    case RET:
                        ret(&reg.SP, &mem[reg.SP].brut, &reg.PC);
                        break;

                }
            break;

            /*Mode d'adressage : direct-registre*/
            case DIRREG:
                switch(currentInstru.codage.codeop) {
                    case STORE:
                        store(&reg.gen[currentInstru.codage.source], &mem[secondMot.brut].brut);
                        break;
                    case POP:
                        pop(&mem[secondMot.brut].brut,&reg.SP, &mem[reg.SP].brut);
                        break;
                    case PUSH:
                        push(&mem[reg.SP].brut, &reg.gen[currentInstru.codage.source],&reg.SP);
                        break;
                    case RET:
                        ret(&reg.SP, &mem[reg.SP].brut, &reg.PC);
                        break;
                }
            break;

            /*Mode d'adressage : indirect-registre*/
            case INDREG:
                switch(currentInstru.codage.codeop) {
                    case STORE:
                        store(&reg.gen[currentInstru.codage.source], &mem[reg.gen[currentInstru.codage.dest]].brut);
                        break;
                    case POP:
                        pop(&mem[reg.gen[currentInstru.codage.dest]].brut,&reg.SP, &mem[reg.SP].brut);
                        break;
                    case PUSH:
                        push(&mem[reg.SP].brut, &reg.gen[currentInstru.codage.source],&reg.SP);
                        break;
                    case RET:
                        ret(&reg.SP, &mem[reg.SP].brut, &reg.PC);
                        break;
                }
            break;

            /*Mode d'adressage : registre-immediat*/
            case REGIMM:
                switch(currentInstru.codage.codeop) {
                    case ADD:
                        add(&secondMot.brut, &reg.gen[currentInstru.codage.dest],&reg.SR);
                        break;
                    case SUB:
                        sub(&secondMot.brut, &reg.gen[currentInstru.codage.dest],&reg.SR);
                        break;
                    case LOAD:
                        load(&secondMot.brut ,&reg.gen[currentInstru.codage.dest]);
                        break;
                    case JMP:
                        jmp(&reg.PC, &secondMot.brut);
                        break;
                    case JEQ:
                        jeq(&secondMot.brut, &reg.PC, &reg.SR);
                        break;
                    case CALL:
                        call(&reg.PC, &reg.SP, &secondMot.brut, &mem[reg.SP].brut);
                        break;
                    case POP:
                        pop(&reg.gen[currentInstru.codage.dest],&reg.SP, &mem[reg.SP].brut);
                        break;
                    case PUSH:
                        push(&mem[reg.SP].brut, &secondMot.brut,&reg.SP);
                        break;
                    case RET:
                        ret(&reg.SP, &mem[reg.SP].brut, &reg.PC);
                        break;
                }
            break;

            /*Mode d'adressage : direct-immediat*/
            case DIRIMM:
                switch(currentInstru.codage.codeop) {
                    case STORE:
                        store(&secondMot.brut, &mem[troisiemeMot.brut].brut);
                        break;
                    case JMP:
                        jmp(&reg.PC, &secondMot.brut);
                        break;
                    case JEQ:
                        jeq(&secondMot.brut, &reg.PC, &reg.SR);
                        break;
                    case CALL:
                        call(&reg.PC, &reg.SP, &secondMot.brut, &mem[reg.SP].brut);
                        break;
                    case POP:
                        pop(&mem[secondMot.brut].brut,&reg.SP, &mem[reg.SP].brut);
                        break;
                    case PUSH:
                        push(&mem[reg.SP].brut, &secondMot.brut,&reg.SP);
                        break;
                    case RET:
                        ret(&reg.SP, &mem[reg.SP].brut, &reg.PC);
                        break;
                }
            break;

            /*Mode d'adressage : indirect-immediat*/
            case INDIMM:
                switch(currentInstru.codage.codeop) {
                    case STORE:
                        store(&secondMot.brut, &mem[reg.gen[currentInstru.codage.dest]].brut);
                        break;
                    case JMP:
                        jmp(&reg.PC, &secondMot.brut);
                        break;
                    case JEQ:
                        jeq(&secondMot.brut, &reg.PC, &reg.SR);
                        break;
                    case CALL:
                        call(&reg.PC, &reg.SP, &secondMot.brut, &mem[reg.SP].brut);
                        break;
                    case POP:
                        pop(&mem[reg.gen[currentInstru.codage.dest]].brut, &reg.SP, &mem[reg.SP].brut);
                        break;
                    case PUSH:
                        push(&mem[reg.SP].brut, &secondMot.brut,&reg.SP);
                        break;
                    case RET:
                        ret(&reg.SP, &mem[reg.SP].brut, &reg.PC);
                        break;

                }
            break;

            /*Mode d'adressage : registre-direct*/
            case REGDIR:
                switch(currentInstru.codage.codeop) {
                    case ADD:
                        add(&mem[reg.gen[currentInstru.codage.source]].brut, &reg.gen[currentInstru.codage.dest],&reg.SR);
                        break;
                    case SUB:
                        sub(&mem[reg.gen[currentInstru.codage.source]].brut, &reg.gen[currentInstru.codage.dest],&reg.SR);
                        break;
                    case LOAD:
                        load(&mem[secondMot.brut].brut ,&reg.gen[currentInstru.codage.dest]);
                        break;
                    case POP:
                        pop(&reg.gen[currentInstru.codage.dest], &reg.SP, &mem[reg.SP].brut);
                        break;
                    case PUSH:
                        push(&mem[reg.SP].brut, &mem[secondMot.brut].brut,&reg.SP);
                        break;
                    case RET:
                        ret(&reg.SP, &mem[reg.SP].brut, &reg.PC);
                        break;
                }
            break;

            /*Mode d'adressage : registre-indirect*/
            case REGIND:
                switch(currentInstru.codage.codeop) {
                    case ADD:
                        add(&mem[reg.gen[currentInstru.codage.source]].brut, &reg.gen[currentInstru.codage.dest],&reg.SR);
                        break;
                    case SUB:
                        sub(&mem[reg.gen[currentInstru.codage.source]].brut, &reg.gen[currentInstru.codage.dest],&reg.SR);
                        break;
                    case LOAD:
                        load(&mem[reg.gen[currentInstru.codage.source]].brut ,&reg.gen[currentInstru.codage.dest]);
                        break;
                    case POP:
                        pop(&reg.gen[currentInstru.codage.dest], &reg.SP, &mem[reg.SP].brut);
                        break;
                    case PUSH:
                        push(&mem[reg.SP].brut, &mem[reg.gen[currentInstru.codage.source]].brut,&reg.SP);
                        break;
                    case RET:
                        ret(&reg.SP, &mem[reg.SP].brut, &reg.PC);
                        break;
                }
            break;
        }

    }
    return 0;
}

/*V�rifie que le mode d'adressage soit bien valide pour l'instruction donn�e.*/
int checkModeInstru(mot instru) {
    if(instru.codage.codeop == LOAD && (instru.codage.mode != REGDIR && instru.codage.mode != REGIMM && instru.codage.mode != REGIND)) return 1;
    if(instru.codage.codeop == STORE && (instru.codage.mode != DIRIMM && instru.codage.mode != DIRREG && instru.codage.mode != INDIMM && instru.codage.mode != INDREG )) return 1;
    if((instru.codage.codeop == ADD || instru.codage.codeop == SUB) && (instru.codage.mode != REGREG && instru.codage.mode != REGIMM && instru.codage.mode != REGDIR && instru.codage.mode != REGIND)) return 1;
    if((instru.codage.codeop == JMP || instru.codage.codeop == JEQ || instru.codage.codeop == CALL ) && (instru.codage.mode != DIRIMM && instru.codage.mode != REGIMM && instru.codage.mode != INDIMM && instru.codage.mode != INDREG )) return 1;

    return 0;
}

