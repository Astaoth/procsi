Projet Emulateur Procsi

auteurs : Alexandre Schifano, Axel Vicard

===================================# Fichiers actuels

codage.h : contient toutes les codages des codes op et des modes d'adressage. Ainsi, ADD correspond au champ de bit 000000
dans un fichier s�par� pour pls de lisibilit�
dans un .h pour �tre inclu plus facilement
codes op manquant : d�finis arbitrairement - cel� n'impacte pas le fonctionnement de l'�mulateur

memory.c : pour le moment, copier-coller du pdf

main.c : n�ant.


================================# Notes de dev :

procsi a une archi 16bits
=> chacun des registres fait 16b, de m�me que chacune des cases m�moires
=>un programme ne peut pas contenir plus de 65'535 (2^16-1) mots, sinon on d�passe la taille de PC

registres :
R0-7 : 8 regitres g�n�raux
PC : adresse de l'instru en cours
SP : adresse du sommet de la pile d'exec
SR : reslt de la derni�re instru

La m�moire :
elle est en r�alit� compos� de 2 tableaux :
    la m�moire des instrus
    la m�moire des donn�es
        par d�faut, on consid�re que l'adresse de d�part de cette meme est 1000


*un fichier (function.c ?) contenant toutes les fct de procsi, en C
ex : int add( ... )

le chargement de la m�moire se fait directement dans la fonction de procsi, ainsi que le reste du traitement ; rien n'emp�che de faire d'autres fonctions appel�es dans les premi�res pour r�p�ter le moins possible le code

*archi sug�r�e :
main.c : le coeur
display.c : la gestion de la partie affichage
memory.c : contiendra les diff�rents programmes asm, et servira � les charger en fonction de la demande de l'user
memory.h : en plus des prototypes, contient les structures de m�moire, avec une macro servant � �viter de les d�finir plusieurs fois
function.c : une lib des fonctions de procsi
function.h : le plus logique est, je pense, de d�finir les codages ici, avec le mm principe de macro que ci-dessus.
    Pourquoi les d�finir ici ? Parce qu'ils sont principalement utilis�s dans function.c, pour garder un main.h clair, et pour ne pas avoir un fichier servant juste � d�finir quelques macros

===============================# Taille des variables
Au vu de l'�nonc�, on admettra qu'un short fait 2 octets, soit 16b

===============================#Tour de lecture d'une instru
lecture de l'instruction (� l'adresse PC) - fait
incr�mente PC - fait
parse l'instru - se fait � plusieurs endroit *une premi�re fois ici pour le mode (et donc savoir si il y a d'autres mots)
charge du second mot si n�cessaire - fait
charge du troisi�me mot si n�cessaire - fait
charge de la m�moie vers des registres g�n�raux - � part dans le cas de load, ne sert � rien
ex�cution de l'instru, l'instru est pars�e une seconde fois
�criture du r�sultat : modifier l'op�rande de dst, ou modifier PC pour les instrus de controle
    se fait durant le calcul, sauf si dst vient de la m�moire
