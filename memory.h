/*Définition des structures utilisées pour gérer la mémoire et les registres.*/

#ifndef STRUCT_MEMORY
#define STRUCT_MEMORY

/*Structure contenant l'ensemble des registres*/
typedef struct {
        short PC;
        short SP;
        short SR;
        short gen[8];
} registre;

/*Défini une case mémoire.
Une case mémoire est :
_soit une instru avec son code op, son mode, et ses éventuelles source et destination,
_soit un mot brut à utiliser en combinaison avec une instru asm*/
typedef union
{
    short brut;
    struct
    {
        unsigned short codeop : 6;
        unsigned short mode : 4;
        unsigned short source : 3;
        unsigned short dest : 3;

    }codage;

} mot;

#endif

/*Prototypes des fonctions de memory.c*/
#ifndef FCT_MEMORY
#define FCT_MEMORY
/*fonction de chargement des différents programmes*/
void loadMem1(mot* mem);
#endif
