/* Définition des macros des codages*/

#ifndef CODE_FUNCTION
#define CODE_FUNCTION

/*code-op*/
#define ADD 0
#define SUB 1
#define JMP 4
#define CALL 5
#define LOAD 8
#define STORE 9
#define POP 16
/*défini arbitrairement*/
#define RET 20
#define PUSH 21
#define HALT 22
#define JEQ 23



/*mode d'adressage*/
#define REGREG 0
#define DIRREG 1
#define INDREG 2
#define REGIMM 4
#define DIRIMM 5
#define INDIMM 6
#define REGDIR 8
#define REGIND 12

#endif

/*Prototypes des fonctions de function.c*/
#ifndef FCT_FUNCTION
#define FCT_FUNCTION
void add(short *src, short *dst, short *SR);
void sub(short *src, short *dst, short *SR);
void jmp(short *PC, short *src);
void call(short *PC, short *SP, short *src, short *mem);
void ret(short* SP, short* mem, short* PC);
void push(short* mem, short* src, short* SP);
void load(short *src,short *dst);
void store(short *src,short *dst);
void jeq(short* src,short *PC, short *SR);
void pop(short* dst, short *SP,short *mem);

#endif

