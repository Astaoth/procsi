#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "memory.h"
#include "function.h"
#include "display.h"

/*On consid�re qu'une console a au moins 80 caract�res de large*/

/*Fonciton d'affichage des registres*/
void printReg(registre reg) {
    /*registres g�n�raux*/
    printf("############################\n");
    printf("|| \n");
    printf("|| REGISTRES GENERAUX\n");
    printf("|| \n");
    printf("++====+==================\n");
    printf("||    |\n");
    printf("|| R0 =  %d\n",reg.gen[0]);
    printf("||    |\n");
    printf("++----+------------------\n");
    printf("||    |\n");
    printf("|| R1 =  %d\n",reg.gen[1]);
    printf("||    |\n");
    printf("++----+------------------\n");
    printf("||    |\n");
    printf("|| R2 =  %d\n",reg.gen[2]);
    printf("||    |\n");
    printf("++----+------------------\n");
    printf("||    |\n");
    printf("|| R3 =  %d\n",reg.gen[3]);
    printf("||    |\n");
    printf("++----+------------------\n");
    printf("||    |\n");
    printf("|| R4 =  %d\n",reg.gen[4]);
    printf("||    |\n");
    printf("++----+------------------\n");
    printf("||    |\n");
    printf("|| R5 =  %d\n",reg.gen[5]);
    printf("||    |\n");
    printf("++----+------------------\n");
    printf("||    |\n");
    printf("|| R6 =  %d\n",reg.gen[6]);
    printf("||    |\n");
    printf("++----+------------------\n");
    printf("||    |\n");
    printf("|| R7 =  %d\n",reg.gen[7]);
    printf("||    |\n");
    printf("############################\n\n\n");

    /*registres sp�cialis�s*/
    printf("############################\n");
    printf("|| \n");
    printf("|| REGISTRES SPECIALISES\n");
    printf("|| \n");
    printf("++====+==================\n");
    printf("||    |\n");
    printf("|| PC =  %d\n",reg.PC);
    printf("||    |\n");
    printf("++----+------------------\n");
    printf("||    |\n");
    printf("|| SP =  %d\n",reg.SP);
    printf("||    |\n");
    printf("++----+------------------\n");
    printf("||    |\n");
    printf("|| SR =  %d\n",reg.SR);
    printf("||    |\n");
    printf("############################\n\n\n");
    printf("Appuyez sur \"Entrer\" pour continuer ...");
    getchar();
    fflush(stdin);
}

void printInstru(mot instru, mot mot2, mot mot3, int boolMot2, int boolMot3) {
    printf("++=======================\n");
    printf("||             |\n");
    printf("|| Instruction =  codeop : %d - mode : %d - source : %d - dest : %d\n",instru.codage.codeop, instru.codage.mode, instru.codage.source, instru.codage.dest);
    printf("||             |\n");

    if(boolMot2) {
        printf("++-----------------------\n");
        printf("||            |\n");
        printf("|| Second mot =  %d\n",mot2.brut);
        printf("||            |\n");

    }

    if(boolMot3) {
        printf("++-----------------------\n");
        printf("||               |\n");
        printf("|| Troisi�me mot =  %d\n",mot3.brut);
        printf("||               |\n");

    }

    printf("++=======================\n\n\n");

    printf("Appuyez sur \"Entrer\" pour continuer ...");
    getchar();
    fflush(stdin);

}
