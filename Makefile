main: display.o memory.o function.o main.h
	gcc -pedantic -Wall -ansi main.c -o procsi

memory: function.o memory.h
	gcc -pedantic -Wall -ansi memory.c -o memory.o

function: function.h
	gcc -pedantic -Wall -ansi function.c -o function.o

display: memory.o function.o display.h
    gcc -pedantic -Wall -ansi display.c -o display.o
