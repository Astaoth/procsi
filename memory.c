#include <stdio.h>
#include <stdlib.h>
#include "function.h"
#include "memory.h"

/*ex du pdf
mot mem[TAILLE_MEM] = {
    {.codage = {ADD,REGREG,3,4}},
    {.codage = {LOAD,REGIND,3,4}},
    {.brut = 1000},
    {.codage = {JMP,REGIMM}},
    {.brut = 1500}
};*/

void loadMem1(mot * mem) {
    int i;
    mot tmpMem[8]= {
        {.codage = {LOAD,REGIMM,0,0}},
        {.brut = 10},
        {.codage = {LOAD,REGIMM,0,1}},
        {.brut = 10},
        {.codage = {ADD,REGREG,0,1}},
        {.codage = {STORE,DIRREG,1}},
        {.brut = 1000},
        {.codage = {HALT}}
    };

    mem = malloc(sizeof(tmpMem));
    for(i=0 ; i < sizeof(tmpMem) ; i++) mem[i] = tmpMem[i];
}

