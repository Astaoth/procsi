#include <stdio.h>
#include <stdlib.h>
#include "memory.h"
#include "function.h"

/*Contient l'ensemble des fonctions de Procsi*/

/*Toutes les fonctions de procsi chargent elles-mêmes les données en fonction de la source, de la destination, et du mode*/

void add(short *src, short *dst, short *SR) {
    *dst += *src;
    *SR = *dst;
}

void sub(short *src, short *dst, short *SR) {
    *dst -= *src;
    *SR = *dst;
}

void jmp(short *PC, short *src) {
    *PC = *src;
}

void call(short *PC, short *SP, short *src, short *mem) {
    *mem = *PC;
    *SP -= 1;
    *PC = *src;
}

void ret(short* SP, short* mem, short* PC) {
    *SP+=1;
    *PC = *mem;
}

void push(short* mem, short* src, short* SP) {
    *mem = *src;
    *SP -=1;
}

void load(short *src,short *dst){
    *dst=*src;
}

void store(short *src,short *dst){
    *dst=*src;
}

void jeq(short* src,short *PC, short *SR){
    if(*SR==0) *PC=*src;
}

void pop(short* dst, short *SP,short *mem){
    *SP+=1;
    *dst=*mem;
}

